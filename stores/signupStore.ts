import { defineStore } from 'pinia';
import { ISignupUser } from '@/shared/types';

interface IStore {
  userData: ISignupUser;
}

export const useSignupStore = defineStore('signup', {
  state: (): IStore => {
    return {
      userData: {
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
        accept: false,
      },
    };
  },
  getters: {
    isFilledForm(state) {
      return Object.values(state.userData).filter(Boolean).length > 0;
    },
  },
  actions: {
    resetPassword() {
      this.userData = { ...this.userData, password: '', confirmPassword: '' };
    },
  },
});
