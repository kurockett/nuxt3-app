import { defineStore } from 'pinia';
import { ILoginUser } from '~/shared/types';

interface IStore {
  userData: ILoginUser;
}

export const useLoginStore = defineStore('login', {
  state: (): IStore => {
    return {
      userData: {
        email: '',
        password: '',
      },
    };
  },
});
