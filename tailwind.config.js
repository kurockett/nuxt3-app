/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './shared/**/*.{ts,vue}',
  ],
  theme: {
    colors: {
      white: '#ffffff',
      black: '#09090B',
      dark: '#161616',
      grey: '#71717A',
      'white-100': '#F5F8FF',
      'primary-blue': '#6172F3',
      'secondary-blue': '#8098F9',
      'secondary-blue-opacity': '#8098f980',
    },
    extend: {
      fontFamily: {
        inter: ['Inter', 'sans-serif'],
        montserrat: ['Montserrat', 'sans-serif'],
      },
      gridTemplateColumns: {
        otp: 'repeat(6, minmax(0, 4rem))',
      },
    },
  },
  plugins: [],
};
