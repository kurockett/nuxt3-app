// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: process.env.NODE_ENV === 'development' },
  css: [
    '@/shared/assets/stylesheets/main.css',
    '@/shared/assets/stylesheets/fonts.css',
    '@/shared/assets/stylesheets/ui.css',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  routeRules: {
    '/': {
      redirect: '/login',
    },
  },
  modules: ['@pinia/nuxt'],
  pinia: {
    storesDirs: ['./stores/**'],
  },
});
