export interface ILoginUser {
  email: string;
  password: string;
}

export interface ISignupUser extends ILoginUser {
  username: string;
  confirmPassword: string;
  accept: boolean;
}
