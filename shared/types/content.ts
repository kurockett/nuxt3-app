export interface IContent {
  [key: string]: {
    title: string;
    description: string;
    imageSrc: string;
  };
}
