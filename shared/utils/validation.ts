export const isValidForm = <T extends object>(formData: T): boolean =>
  Object.values(formData).every(Boolean);
